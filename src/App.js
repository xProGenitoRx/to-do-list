import React, {useState} from 'react';
import { nanoid } from 'nanoid';
import Todo from './components/Todo.js';
import Form from './components/Form.js';
import FilterButton from './components/FilterButton.js';

const FILTER_MAP = {
  All: _=> true,
  Active: task => !task.completed,
  Completed: task => task.completed,
}
const FILTER_NAMES = Object.keys (FILTER_MAP);

function App(props) {
  const [tasks, setTasks] = useState (props.tasks);
  const [filter, setFilter] = useState ('All');

  const taskList = tasks
  .filter(FILTER_MAP[filter])
  .map ( task => (
    <Todo name={task.name} 
          completed={task.completed} 
          id={task.id}
          key={task.id}
          toggleTaskCompleted={toggleTaskCompleted}
          deleteTask={deleteTask}
          editTask={editTask} /> ));

  const filterList = FILTER_NAMES.map ( name=> (
    <FilterButton
      key={name}
      name={name}
      isPressed = {name === filter}
      setFilter = {setFilter}
    />
  ));

  const tasksNoun = taskList.length !== 1 ? 'tasks' : 'task'
  const headingText = `${taskList.length} ${tasksNoun} remaining`;

  function addTask (name) {
    const newTask = {id: 'todo-'+nanoid(), name: name, completed: false,};
    setTasks ([...tasks, newTask]);
  }
  
  function deleteTask (id) {
    const updatedTask = tasks.filter ( task => id !== task.id );
    setTasks (updatedTask);
  }

  function editTask(id, newName) {
    const editedTask = tasks.map ( task => id === task.id ? {...task, name: newName} : task );
    setTasks (editedTask);
  }

  function toggleTaskCompleted (id) {
    const updatedTask = tasks.map ( task => {
      if (task.id === id) {
        return {...task, completed: !task.completed};
      }
      return task;
    });
    setTasks (updatedTask);
  }

  return (
    <div className="todoapp stack-large">
      <h1>TodoMatic</h1>
      <Form addTask={addTask}/>
      <div className="filters btn-group stack-exception">
        {filterList}
      </div>
      <h2 id="list-heading">{headingText}</h2>
      <ul
        role="list"
        className="todo-list stack-large stack-exception"
        aria-labelledby="list-heading"
      >
        {taskList}
      </ul>
    </div>
  );
}

export default App;
